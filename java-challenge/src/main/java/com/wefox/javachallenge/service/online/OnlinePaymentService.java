package com.wefox.javachallenge.service.online;

public interface OnlinePaymentService {

    void processOnlinePayment(String message);
}
