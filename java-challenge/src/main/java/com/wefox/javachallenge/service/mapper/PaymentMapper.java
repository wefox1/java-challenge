package com.wefox.javachallenge.service.mapper;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEnum;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.service.vo.PaymentVO;

public class PaymentMapper {
    private final AccountRepository accountRepository;
    private final ErrorRepository errorRepository;

    @Autowired
    public PaymentMapper(AccountRepository accountRepository,
            ErrorRepository errorRepository) {
        this.accountRepository = accountRepository;
        this.errorRepository = errorRepository;
    }

    public PaymentEntity mapPaymentMessageToPaymentEntity(String message) {
        return mapPaymentVoToPaymentEntity(mapPaymentMessageToPaymentVo(message));
    }

    public PaymentVO mapPaymentMessageToPaymentVo(String message){
        try{
            return new ObjectMapper().readValue(message, PaymentVO.class);

        } catch(JsonProcessingException e) {
            errorRepository.saveOtherError(null, "Unparseable payment");
            return null;
        }
    }

    public PaymentEntity mapPaymentVoToPaymentEntity(PaymentVO paymentVo) {
        if(paymentVo == null) {
            return null;
        }

        PaymentEntity paymentEntity = new PaymentEntity();

        paymentEntity.setPaymentId(paymentVo.getPaymentId());
        paymentEntity.setAccountEntity(getAccountEntity(paymentVo));
        paymentEntity.setPaymentType(paymentVo.getPaymentType());
        paymentEntity.setCreditCard(paymentVo.getCreditCard());
        paymentEntity.setAmount(Integer.parseInt(paymentVo.getAmount()));
        paymentEntity.setCreatedAt(LocalDate.now());

        return paymentEntity;
    }

    private AccountEntity getAccountEntity(PaymentVO paymentVo) {
        if (paymentVo.getPaymentType().equals(PaymentEnum.OFFLINE.getPaymentType())) {
            return null;
        } else {
            Optional<AccountEntity> accountModel =
                    accountRepository.findById(Integer.parseInt(paymentVo.getAccountId()));

            return accountModel.orElse(null);
        }
    }
}