package com.wefox.javachallenge.service.offline;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.kafka.annotation.KafkaListener;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.PaymentRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.service.mapper.PaymentMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OfflinePaymentServiceImpl implements OfflinePaymentService{
    private static final String OFFLINE_TOPIC = "offline";
    private static final String GROUP_ID = "payment";

    private final AccountRepository accountRepository;
    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;
    private final ErrorRepository errorRepository;

    public OfflinePaymentServiceImpl(AccountRepository accountRepository,
            PaymentRepository paymentRepository,
            PaymentMapper paymentMapper,
            ErrorRepository errorRepository){
        this.accountRepository = accountRepository;
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
        this.errorRepository = errorRepository;
    }

    @Override
    @KafkaListener(topics = OFFLINE_TOPIC, groupId = GROUP_ID)
    public void processOfflinePayment(String message){
        PaymentEntity paymentEntity = paymentMapper.mapPaymentMessageToPaymentEntity(message);

        if(paymentEntity != null){
            savePaymentInDatabase(paymentEntity);
            updateAccount(paymentEntity);
        }

    }

    private void savePaymentInDatabase(PaymentEntity paymentEntity){
            paymentRepository.save(paymentEntity);
    }

    private void updateAccount(PaymentEntity paymentEntity){
        AccountEntity accountEntity = paymentEntity.getAccountEntity();

        if(accountEntity != null){
            LocalDate updatedLastPaymentDate = getPaymentCreatedOnStringDate(paymentEntity);
            accountEntity.setLastPaymentDate(updatedLastPaymentDate);
            saveAccountInDatabase(accountEntity);
            accountRepository.findById(accountEntity.getAccountId());

        }else{
            errorRepository.saveDatabaseError(paymentEntity.getPaymentId(), "No account associated found");

        }

    }

    private LocalDate getPaymentCreatedOnStringDate(PaymentEntity paymentEntity){
        Optional<PaymentEntity> paymentEntityOptional = paymentRepository.findById(paymentEntity.getPaymentId());
        return paymentEntityOptional.isEmpty() ? null : paymentEntityOptional.get().getCreatedAt();
    }

    private void saveAccountInDatabase(AccountEntity accountEntity){
        accountRepository.save(accountEntity);
    }

}