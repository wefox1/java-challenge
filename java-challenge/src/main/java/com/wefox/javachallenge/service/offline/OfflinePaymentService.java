package com.wefox.javachallenge.service.offline;

public interface OfflinePaymentService {
    void processOfflinePayment(String message);

}