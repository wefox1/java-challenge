package com.wefox.javachallenge.service.mapper;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;
import com.wefox.javachallenge.repository.rest.error.dto.ErrorEnum;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ErrorMapper{

    public ErrorDto createDatabaseError(String paymentId, String errorDescription){
        return new ErrorDto(paymentId, ErrorEnum.DATABASE, errorDescription);
    }

    public ErrorDto createNetworkError(String paymentId, String errorDescription){
        return new ErrorDto(paymentId, ErrorEnum.NETWORK, errorDescription);
    }

    public ErrorDto createOtherError(String paymentId, String errorDescription){
        return new ErrorDto(paymentId, ErrorEnum.OTHER, errorDescription);
    }
}