package com.wefox.javachallenge.service.online;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.kafka.annotation.KafkaListener;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.PaymentRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.repository.rest.paymentvalidator.PaymentValidatorRepository;
import com.wefox.javachallenge.service.mapper.PaymentMapper;
import com.wefox.javachallenge.service.vo.PaymentVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OnlinePaymentServiceImpl implements OnlinePaymentService {
    private static final String ONLINE_TOPIC = "online";
    private static final String GROUP_ID = "payment";

    private final AccountRepository accountRepository;
    private final PaymentRepository paymentRepository;
    private final PaymentMapper paymentMapper;

    private final PaymentValidatorRepository paymentValidatorRepository;
    private final ErrorRepository errorRepository;

    public OnlinePaymentServiceImpl(AccountRepository accountRepository,
                                    PaymentRepository paymentRepository,
                                    PaymentMapper paymentMapper,
                                    ErrorRepository errorRepository,
                                    PaymentValidatorRepository paymentValidatorRepository) {
        this.accountRepository = accountRepository;
        this.paymentRepository = paymentRepository;
        this.paymentMapper = paymentMapper;
        this.errorRepository = errorRepository;
        this.paymentValidatorRepository = paymentValidatorRepository;
    }

    @Override
    @KafkaListener(topics = ONLINE_TOPIC, groupId = GROUP_ID)
    public void processOnlinePayment(String message){
            PaymentVO paymentVo = paymentMapper.mapPaymentMessageToPaymentVo(message);

            if(paymentValidatorRepository.isPaymentValid(paymentVo)){
                PaymentEntity paymentEntity = paymentMapper.mapPaymentVoToPaymentEntity(paymentVo);
                savePaymentInDatabase(paymentEntity);
                updateAccount(paymentEntity);

            }else{
                String paymentId = paymentVo != null ? paymentVo.getPaymentId() : null;
                errorRepository.saveDatabaseError(paymentId, "Payment not valid");
            }

    }

    private void savePaymentInDatabase(PaymentEntity paymentEntity) {
        paymentRepository.save(paymentEntity);
    }

    private void updateAccount(PaymentEntity paymentEntity) {
        AccountEntity accountEntity = paymentEntity.getAccountEntity();

        if (accountEntity != null) {
            LocalDate updatedLastPaymentDate = getPaymentCreatedAt(paymentEntity);
            accountEntity.setLastPaymentDate(updatedLastPaymentDate);
            saveAccountInDatabase(accountEntity);

            accountRepository.findById(accountEntity.getAccountId());

        } else {
            errorRepository.saveDatabaseError(paymentEntity.getPaymentId(), "No account associated found");

        }

    }

    private LocalDate getPaymentCreatedAt(PaymentEntity paymentEntity) {
        Optional<PaymentEntity> paymentEntityOptional = paymentRepository.findById(paymentEntity.getPaymentId());
        return paymentEntityOptional.isEmpty() ? null : paymentEntityOptional.get().getCreatedAt();
    }

    private void saveAccountInDatabase(AccountEntity accountEntity) {
        accountRepository.save(accountEntity);
    }

}