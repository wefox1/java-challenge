package com.wefox.javachallenge.repository.rest.error;

public interface ErrorRepository {

    void saveNetworkError(String paymentId, String message);
    void saveDatabaseError(String paymentId, String message);
    void saveOtherError(String paymentId, String message);

}
