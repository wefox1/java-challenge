package com.wefox.javachallenge.repository.rest.error.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class ErrorDto{

    private String paymentId;
    private String error;
    private String errorDescription;

    public ErrorDto(String paymentId, ErrorEnum error, String errorDescription){
        this.paymentId = paymentId;
        this.error = error.getError();
        this.errorDescription = errorDescription;
    }
}