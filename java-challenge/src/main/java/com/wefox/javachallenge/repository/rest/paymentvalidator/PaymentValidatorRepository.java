package com.wefox.javachallenge.repository.rest.paymentvalidator;

import com.wefox.javachallenge.service.vo.PaymentVO;

public interface PaymentValidatorRepository{

    boolean isPaymentValid(PaymentVO paymentVo);
}
