package com.wefox.javachallenge.repository.rest.paymentvalidator;

import com.wefox.javachallenge.service.vo.PaymentVO;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PaymentValidatorRepositoryImpl implements PaymentValidatorRepository{

    private final PaymentValidatorWebClient paymentValidatorWebClient;

    @Override
    public boolean isPaymentValid(PaymentVO paymentVo){
        return paymentVo != null &&
                paymentValidatorWebClient.validatePayment(paymentVo).is2xxSuccessful();
    }
}
