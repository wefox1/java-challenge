package com.wefox.javachallenge.repository.rest.error;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;
import com.wefox.javachallenge.service.mapper.ErrorMapper;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ErrorRepositoryImpl implements ErrorRepository{

    private final ErrorMapper errorMapper;
    private final ErrorWebClient errorWebClient;


    @Override
    public void saveNetworkError(String paymentId, String message){
        ErrorDto errorDto = errorMapper.createNetworkError(paymentId,message);
        errorWebClient.logError(errorDto);
    }

    @Override
    public void saveDatabaseError(String paymentId, String message){
        ErrorDto errorDto = errorMapper.createDatabaseError(paymentId,message);
        errorWebClient.logError(errorDto);
    }

    @Override
    public void saveOtherError(String paymentId, String message){
        ErrorDto errorDto = errorMapper.createOtherError(paymentId,message);
        errorWebClient.logError(errorDto);
    }
}
