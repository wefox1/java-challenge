package com.wefox.javachallenge.repository.db.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "accounts")
public class AccountEntity{
    
    @Id
    private Integer accountId;
    private String email;
    private LocalDate birthdate;
    private LocalDate lastPaymentDate;
    @Column(name = "created_on")
    private LocalDate createdAt;
}