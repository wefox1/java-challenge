package com.wefox.javachallenge.repository.rest.error.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorEnum {

    DATABASE("database"),
    NETWORK("network"),
    OTHER("other");

    private final String error;
}