package com.wefox.javachallenge.repository.db.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "payments")
public class PaymentEntity{

    @Id
    private String paymentId;

    @OneToOne
    @JoinColumn(name = "accountId")
    private AccountEntity accountEntity;

    private String paymentType;
    private String creditCard;
    private Integer amount;
    @Column(name = "created_on")
    private LocalDate createdAt;
}