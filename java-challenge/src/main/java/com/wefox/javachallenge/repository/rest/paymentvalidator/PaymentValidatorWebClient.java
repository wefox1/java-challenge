package com.wefox.javachallenge.repository.rest.paymentvalidator;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.wefox.javachallenge.service.vo.PaymentVO;

import lombok.NoArgsConstructor;
import reactor.core.publisher.Mono;

@NoArgsConstructor
public class PaymentValidatorWebClient{

    @Value("${webclient.payment.gateway.url}")
    private String paymentGatewayProviderUrl;

    public HttpStatus validatePayment(PaymentVO paymentVO){
        try{
            return WebClient.create()
                    .post()
                    .uri(new URI(paymentGatewayProviderUrl))
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(paymentVO)
                    .exchangeToMono(clientResponse -> Mono.just(clientResponse.statusCode()))
                    .blockOptional()
                    .get();
        }catch(URISyntaxException e){
            throw new RuntimeException(e);
        }
    }
}