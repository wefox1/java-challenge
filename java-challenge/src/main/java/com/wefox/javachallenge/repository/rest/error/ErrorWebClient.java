package com.wefox.javachallenge.repository.rest.error;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ErrorWebClient{

    @Value("${webclient.log.gateway.url}")
    private String errorLogProviderUrl;

    public void logError(ErrorDto errorDto){
        try{
            WebClient.create()
                    .post()
                    .uri(new URI(errorLogProviderUrl))
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(errorDto);

        }catch(URISyntaxException e){
            throw new RuntimeException(e);
        }
    }
}