package com.wefox.javachallenge.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.PaymentRepository;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.repository.rest.error.ErrorRepositoryImpl;
import com.wefox.javachallenge.repository.rest.error.ErrorWebClient;
import com.wefox.javachallenge.repository.rest.paymentvalidator.PaymentValidatorRepository;
import com.wefox.javachallenge.repository.rest.paymentvalidator.PaymentValidatorRepositoryImpl;
import com.wefox.javachallenge.repository.rest.paymentvalidator.PaymentValidatorWebClient;
import com.wefox.javachallenge.service.mapper.ErrorMapper;
import com.wefox.javachallenge.service.mapper.PaymentMapper;
import com.wefox.javachallenge.service.offline.OfflinePaymentService;
import com.wefox.javachallenge.service.offline.OfflinePaymentServiceImpl;
import com.wefox.javachallenge.service.online.OnlinePaymentService;
import com.wefox.javachallenge.service.online.OnlinePaymentServiceImpl;

@Configuration
public class JavaChallengeBeanConfiguration{

    @Bean
    public OfflinePaymentService offlinePaymentService(AccountRepository accountRepository,
            PaymentRepository paymentRepository,
            PaymentMapper paymentMapper,
            ErrorRepository errorRepository){
        return new OfflinePaymentServiceImpl(accountRepository, paymentRepository, paymentMapper, errorRepository);
    }

    @Bean
    public OnlinePaymentService onlinePaymentService(AccountRepository accountRepository,
            PaymentRepository paymentRepository,
            PaymentMapper paymentMapper,
            ErrorRepository errorRepository,
            PaymentValidatorRepository paymentValidatorRepository){
        return new OnlinePaymentServiceImpl(accountRepository, paymentRepository, paymentMapper, errorRepository,
                paymentValidatorRepository);
    }

    @Bean
    public ErrorMapper errorMapper(){
        return new ErrorMapper();
    }

    @Bean
    public PaymentMapper paymentMapper(AccountRepository accountRepository,
            ErrorRepository errorRepository) {
        return new PaymentMapper(accountRepository, errorRepository);
    }

    @Bean
    public ErrorRepository errorRepository(ErrorMapper errorMapper,
            ErrorWebClient errorWebClient) {
        return new ErrorRepositoryImpl(errorMapper, errorWebClient);
    }

    @Bean
    public ErrorWebClient errorWebClient(){
        return new ErrorWebClient();
    }

    @Bean
    public PaymentValidatorRepository paymentValidatorRepository(PaymentValidatorWebClient paymentValidatorWebClient){
        return new PaymentValidatorRepositoryImpl(paymentValidatorWebClient);
    }

    @Bean
    public PaymentValidatorWebClient paymentValidatorWebClient(){
        return new PaymentValidatorWebClient();
    }

}
