package com.wefox.javachallenge.repository.rest.error;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;

@ExtendWith(MockitoExtension.class)
public class ErrorWebClientTest{

    @InjectMocks
    private ErrorWebClient errorWebClient;
    @Mock
    private WebClient webClient;

    private static ErrorDto errorDto;

    @BeforeEach
    public void init() throws URISyntaxException{
        errorDto = new ErrorDto();

    }

    @Test
    @Disabled
    public void shouldLogError() {
        errorWebClient.logError(errorDto);

        verify(webClient, times(1)).post();
    }
}
