package com.wefox.javachallenge.repository.rest.paymentvalidator;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;

import com.wefox.javachallenge.service.vo.PaymentVO;

@ExtendWith(MockitoExtension.class)
public class PaymentValidatorRepositoryImplTest{

    @InjectMocks
    private PaymentValidatorRepositoryImpl paymentValidatorRepository;
    @Mock
    private PaymentValidatorWebClient paymentValidatorWebClient;

    private PaymentVO paymentVo;

    @BeforeEach
    public void init() {
        paymentVo = new PaymentVO();

    }

    @Test
    public void shouldIsPaymentCorrectReturnTrue(){
        when(paymentValidatorWebClient.validatePayment(paymentVo)).thenReturn(HttpStatus.OK);

        boolean result = paymentValidatorRepository.isPaymentValid(paymentVo);

        verify(paymentValidatorWebClient, times(1)).validatePayment(paymentVo);
        Assertions.assertEquals(true, result);
    }

    @Test
    public void shouldIsPaymentCorrectReturnFalse(){
        boolean result = paymentValidatorRepository.isPaymentValid(null);

        verify(paymentValidatorWebClient, times(0)).validatePayment(paymentVo);
        Assertions.assertEquals(false, result);
    }

}
