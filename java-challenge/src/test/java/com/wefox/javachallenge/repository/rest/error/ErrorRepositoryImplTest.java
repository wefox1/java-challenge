package com.wefox.javachallenge.repository.rest.error;

import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;
import com.wefox.javachallenge.service.mapper.ErrorMapper;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ErrorRepositoryImplTest{

    @InjectMocks
    private ErrorRepositoryImpl errorRepository;

    @Mock
    private ErrorMapper errorMapper;
    @Mock
    private ErrorWebClient errorWebClient;

    private static final String paymentId = "paymentId1";
    private static final String message = "Error message";
    private static ErrorDto errorDto;

    @BeforeAll
    public static void init() {
        errorDto = new ErrorDto();
    }

    @Test
    public void shouldSaveNetWorkError() {
        when(errorMapper.createNetworkError(anyString(), anyString())).thenReturn(errorDto);

        errorRepository.saveNetworkError(paymentId,message);

        verify(errorMapper, times(1)).createNetworkError(paymentId,message);
        verify(errorWebClient, times(1)).logError(errorDto);
    }

    @Test
    public void shouldSaveOtherError() {
        when(errorMapper.createOtherError(anyString(), anyString())).thenReturn(errorDto);

        errorRepository.saveOtherError(paymentId,message);

        verify(errorMapper, times(1)).createOtherError(paymentId,message);
        verify(errorWebClient, times(1)).logError(errorDto);
    }

    @Test
    public void shouldSaveDatabaseError() {
        when(errorMapper.createDatabaseError(anyString(), anyString())).thenReturn(errorDto);

        errorRepository.saveDatabaseError(paymentId,message);

        verify(errorMapper, times(1)).createDatabaseError(paymentId,message);
        verify(errorWebClient, times(1)).logError(errorDto);
    }
}
