package com.wefox.javachallenge.service.online;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.PaymentRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.repository.rest.paymentvalidator.PaymentValidatorRepository;
import com.wefox.javachallenge.service.mapper.PaymentMapper;
import com.wefox.javachallenge.service.vo.PaymentVO;

@ExtendWith(MockitoExtension.class)
public class OnlinePaymentServiceImplTest{

    @InjectMocks
    private OnlinePaymentServiceImpl onlinePaymentService;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private PaymentMapper paymentMapper;
    @Mock
    private ErrorRepository errorRepository;
    @Mock
    private PaymentValidatorRepository paymentValidatorRepository;

    private String message;
    private PaymentVO paymentVo;
    private PaymentEntity paymentEntity;
    private Optional<AccountEntity> accountEntityOptional;
    private AccountEntity accountEntity;
    private LocalDate localDate;

    @BeforeEach
    public void init() {
        message = "{\"payment_id\": \"fdf50f69-a23a-4924-9276-9468a815443a\", \"accountId\": \"1\", \n"
                + "\"payment_type\": \"online\",\"credit_card\": \"12345\",\"amount\":12}";
        localDate = LocalDate.now();
        paymentVo = new PaymentVO("fdf50f69-a23a-4924-9276-9468a815443a","1", "online", "12345", "12");
        accountEntity = new AccountEntity(1, "email@email.com", localDate, localDate, localDate);
        accountEntityOptional = Optional.of(accountEntity);
        paymentEntity = new PaymentEntity("fdf50f69-a23a-4924-9276-9468a815443a", accountEntity, "online", "12345", 12, localDate);

    }

    @Test
    public void shouldProcessOfflinePayment(){
        when(paymentValidatorRepository.isPaymentValid(paymentVo)).thenReturn(true);
        when(paymentMapper.mapPaymentMessageToPaymentVo(message)).thenReturn(paymentVo);
        when(paymentMapper.mapPaymentVoToPaymentEntity(paymentVo)).thenReturn(paymentEntity);
        when(accountRepository.findById(paymentEntity.getAccountEntity().getAccountId())).thenReturn(accountEntityOptional);
        when(paymentRepository.findById(paymentEntity.getPaymentId())).thenReturn(Optional.of(paymentEntity));

        onlinePaymentService.processOnlinePayment(message);

        verify(paymentRepository, times(1)).save(paymentEntity);
        verify(accountRepository, times(1)).save(accountEntity);
    }

    @Test
    public void shouldProcessOfflinePaymentWithPaymentNullOrInvalid(){
        paymentVo = null;
        when(paymentMapper.mapPaymentMessageToPaymentVo(message)).thenReturn(paymentVo);
        when(paymentValidatorRepository.isPaymentValid(paymentVo)).thenReturn(false);

        onlinePaymentService.processOnlinePayment(message);

        verify(paymentRepository, times(0)).save(paymentEntity);
        verify(accountRepository, times(0)).save(accountEntity);
        verify(errorRepository, times(1)).saveDatabaseError(null, "Payment not valid");
    }

    @Test
    public void shouldProcessOfflinePaymentNoAccountFound(){
        paymentEntity = new PaymentEntity("fdf50f69-a23a-4924-9276-9468a815443a", null, "online", "12345", 12, localDate);
        when(paymentValidatorRepository.isPaymentValid(paymentVo)).thenReturn(true);
        when(paymentMapper.mapPaymentMessageToPaymentVo(message)).thenReturn(paymentVo);
        when(paymentMapper.mapPaymentVoToPaymentEntity(paymentVo)).thenReturn(paymentEntity);


        onlinePaymentService.processOnlinePayment(message);

        verify(paymentRepository, times(1)).save(paymentEntity);
        verify(accountRepository, times(0)).save(accountEntity);
        verify(errorRepository, times(1)).saveDatabaseError(paymentEntity.getPaymentId(), "No account associated found");

    }
}
