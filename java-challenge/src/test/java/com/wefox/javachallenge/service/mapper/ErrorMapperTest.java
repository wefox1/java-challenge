package com.wefox.javachallenge.service.mapper;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wefox.javachallenge.repository.rest.error.dto.ErrorDto;
import com.wefox.javachallenge.repository.rest.error.dto.ErrorEnum;

@ExtendWith(MockitoExtension.class)
public class ErrorMapperTest{

    @InjectMocks
    private ErrorMapper errorMapper;

    private String paymentId;
    private String errorDescription;

    @BeforeEach
    public void init() {
        paymentId = "paymentId1";
        errorDescription = "Something gone wrong";
    }

    @Test
    public void shouldCreateDatabaseError() {
        ErrorDto result = errorMapper.createDatabaseError(paymentId, errorDescription);

        Assertions.assertEquals(ErrorEnum.DATABASE.getError(), result.getError());
    }

    @Test
    public void shouldCreateNetworkError() {
        ErrorDto result = errorMapper.createNetworkError(paymentId, errorDescription);

        Assertions.assertEquals(ErrorEnum.NETWORK.getError(), result.getError());
    }

    @Test
    public void shouldCreateOtherError() {
        ErrorDto result = errorMapper.createOtherError(paymentId, errorDescription);

        Assertions.assertEquals(ErrorEnum.OTHER.getError(), result.getError());
    }

}
