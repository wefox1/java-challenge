package com.wefox.javachallenge.service.mapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEnum;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;

@ExtendWith(MockitoExtension.class)
public class PaymentMapperTest{

    @InjectMocks
    private PaymentMapper paymentMapper;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private ErrorRepository errorRepository;

    private String message;
    private PaymentEntity expected;
    private Optional<AccountEntity> accountEntityOptional;
    private AccountEntity accountEntity;

    @BeforeEach
    public void init() {
        message = "{\"payment_id\": \"fdf50f69-a23a-4924-9276-9468a815443a\", \"account_id\": 1,\n"
                + "\"payment_type\": \"online\",\"credit_card\": \"12345\",\"amount\":12}";
        LocalDate localDate = LocalDate.now();
        accountEntity = new AccountEntity(1, "email@email.com", localDate, localDate, localDate);
        accountEntityOptional = Optional.of(accountEntity);
        expected = new PaymentEntity("fdf50f69-a23a-4924-9276-9468a815443a", accountEntity, "online", "12345", 12, localDate);
    }

    @Test
    public void shouldMapPaymentMessageToPaymentEntityReturnOk(){
        when(accountRepository.findById(1)).thenReturn(accountEntityOptional);

        PaymentEntity result = paymentMapper.mapPaymentMessageToPaymentEntity(message);

        assertEquals(expected.getPaymentId(), result.getPaymentId());
        assertEquals(expected.getAccountEntity().getAccountId(), result.getAccountEntity().getAccountId());
        assertEquals(expected.getPaymentType(), PaymentEnum.ONLINE.getPaymentType());
        assertEquals(expected.getPaymentType(), result.getPaymentType());
        assertEquals(expected.getCreditCard(), result.getCreditCard());
        assertEquals(expected.getAmount(), result.getAmount());
        verify(accountRepository, times(1)).findById(1);
    }

    @Test
    public void shouldMapPaymentMessageToPaymentEntityReturnOkOffline(){
        message = message.replaceAll("online", "offline");
        expected.setPaymentType("offline");
        expected.setAccountEntity(null);

        PaymentEntity result = paymentMapper.mapPaymentMessageToPaymentEntity(message);

        assertEquals(expected.getPaymentId(), result.getPaymentId());
        assertEquals(expected.getAccountEntity(), null);
        assertEquals(expected.getPaymentType(), PaymentEnum.OFFLINE.getPaymentType());
        assertEquals(expected.getPaymentType(), result.getPaymentType());
        assertEquals(expected.getCreditCard(), result.getCreditCard());
        assertEquals(expected.getAmount(), result.getAmount());
        verify(accountRepository, times(0)).findById(1);
    }

    @Test
    public void shouldMapPaymentMessageToPaymentEntityReturnError(){
        message = message.substring(0, message.length()-1);

        PaymentEntity result = paymentMapper.mapPaymentMessageToPaymentEntity(message);

        assertEquals(null, result);
        verify(errorRepository, times(1)).saveOtherError(null, "Unparseable payment");
    }


}
