package com.wefox.javachallenge.service.offline;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.wefox.javachallenge.repository.db.AccountRepository;
import com.wefox.javachallenge.repository.db.PaymentRepository;
import com.wefox.javachallenge.repository.db.entity.AccountEntity;
import com.wefox.javachallenge.repository.db.entity.PaymentEntity;
import com.wefox.javachallenge.repository.rest.error.ErrorRepository;
import com.wefox.javachallenge.service.mapper.PaymentMapper;

@ExtendWith(MockitoExtension.class)
public class OfflinePaymentServiceImplTest{

    @InjectMocks
    private OfflinePaymentServiceImpl offlinePaymentService;
    @Mock
    private AccountRepository accountRepository;
    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private PaymentMapper paymentMapper;
    @Mock
    private ErrorRepository errorRepository;

    private String message;
    private PaymentEntity paymentEntity;
    private Optional<AccountEntity> accountEntityOptional;
    private AccountEntity accountEntity;
    private LocalDate localDate;

    @BeforeEach
    public void init() {
        message = "{\"payment_id\": \"fdf50f69-a23a-4924-9276-9468a815443a\", \n"
                + "\"payment_type\": \"offline\",\"credit_card\": \"12345\",\"amount\":12}";
        localDate = LocalDate.now();
        accountEntity = new AccountEntity(1, "email@email.com", localDate, localDate, localDate);
        accountEntityOptional = Optional.of(accountEntity);
        paymentEntity = new PaymentEntity("fdf50f69-a23a-4924-9276-9468a815443a", accountEntity, "offline", "12345", 12, localDate);
    }

    @Test
    public void shouldProcessOfflinePayment(){
        when(paymentMapper.mapPaymentMessageToPaymentEntity(message)).thenReturn(paymentEntity);
        when(accountRepository.findById(paymentEntity.getAccountEntity().getAccountId())).thenReturn(accountEntityOptional);
        when(paymentRepository.findById(paymentEntity.getPaymentId())).thenReturn(Optional.of(paymentEntity));

        offlinePaymentService.processOfflinePayment(message);

        verify(paymentRepository, times(1)).save(paymentEntity);
        verify(accountRepository, times(1)).save(accountEntity);
    }

    @Test
    public void shouldProcessOfflinePaymentWithPaymentNull(){
        when(paymentMapper.mapPaymentMessageToPaymentEntity(message)).thenReturn(null);

        offlinePaymentService.processOfflinePayment(message);

        verify(paymentRepository, times(0)).save(paymentEntity);
        verify(accountRepository, times(0)).save(accountEntity);
    }

    @Test
    public void shouldProcessOfflinePaymentNoAccountFound(){
        paymentEntity = new PaymentEntity("fdf50f69-a23a-4924-9276-9468a815443a", null, "online", "12345", 12, localDate);
        when(paymentMapper.mapPaymentMessageToPaymentEntity(message)).thenReturn(paymentEntity);

        offlinePaymentService.processOfflinePayment(message);

        verify(paymentRepository, times(1)).save(paymentEntity);
        verify(accountRepository, times(0)).save(accountEntity);
        verify(errorRepository, times(1)).saveDatabaseError(paymentEntity.getPaymentId(), "No account associated found");

    }
}
