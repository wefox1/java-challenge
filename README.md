
# Challenge

  

## :computer: How to execute

 For first build just execute the clean install command in the Maven root folder. No third party libraries were added (Lombok is the exception).

> mvn clean install -U -DskipTests

The second step is to run the docker images built with the commands provided. After that you can run the challenge application with a simple Spring boot command:

> ./mvnw spring-boot:run
> 
or
> java JavaChallengeApplication.jar

in the root target folder.

A third option, though, is running the application in your IDE.

## :heavy_check_mark: Testing
Unitary tests and coverage are provided. I recommend using your favourite IDE for detailed coverage information. You can also run the following:

> mvn verify

Also, a basic Sonar Lint has been executed in the main folder of the application. I strongly recommend the use of IDEs for the Sonar report generation. 
 

## :memo: Notes

I have focused in the Spring boot paradigm of **"_standard is better_"**, so I designed the solution with a very basic Three Layer web service approach. Nevertheless, as a Rest Controller layer is no needed, and is not strictly a web service, I used only two layers.

The **Service** layer, is used to handle the message queue requests and apply the **business logic**.

The **Repository** layer, is used to **access** the database and the consumed web services.

This design may be a little old fashioned, but is realiable and **highly scalable**, if you manage the packages correctly.

I also tried to **avoid attribute injection** (nor constructor injection) and a **centralized bean declaration** in a single Spring configuration class. This practice enables Spring to focus resources in other aspects of the application such as: managing Kafka messages and mappings, or handling the JPA repositories.

This also makes dependency management way easier which means that scalability is again improved.

Quality is also important. I provided 86% of coverage with unitary tests. Sadly I did not have time to implement any integration tests.
 

## :pushpin: Things to improve

- Logging. I would have made a proper Logback file which is very useful when debugging or deploying the application. 

- Integration tests. Provided with a Postman collection.

 - A dedicated exception that extends RuntimeException.class with an Error Resolver. I am used to handle this kind of exception in the non-existent Controller layer, in order to return a specific HTTP code (functional error vs. technical error) but I lacked time to implement it and test it.

- Better test coverage. At least 90%.

- Deployment plan. If the application would run in more than a single host, no solution has been provided.